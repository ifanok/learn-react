import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Appdog from './Appdog';

const video = [
	{
		title: 'Video 1', url: 'https://mystorage/video1.jpeg'
	},
	{
		title: 'Video 2', url: 'https://mystorage/video2.jpeg'
	},
	{
		title: 'Video 3', url: 'https://mystorage/video3.jpeg'
	}
]

//ReactDOM.render(<App products={products} />, document.getElementById('root'));
/*ReactDOM.render(
  <Demo data={products} />, document.getElementById('root')
);*/
ReactDOM.render(<Appdog video={video}/>, document.getElementById('root'));